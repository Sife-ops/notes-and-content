## Number Guessing Game 

This is a paired programming exercise. Work with your partner to accomplish the following: 
- Create a “Game” class 
- The game should have a play() method which begins the game 
- There should be a randomly generated number between 0 and 100 which the user should attempt to guess 
- After each guess, the user should be alerted if their guess was too low or too high 
- The user should be able to guess as many times is necessary to guess correctly 
- After the number is guessed correctly, the console should print a success message 


#### Bonus Requirements: 
- Print the number of attempts it took the user to get the correct value along with the success message
- If the value entered is not an integer value, the user should have an appropriate message and the opportunity to guess again 
- If the value entered is out of the “relevant range”, the user should have an appropriate message and the opportunity to guess again (if they guessed 70, and the number was too high and also guessed 30 and the number was too low, 30-70 would be the relevant range)
- As the user guesses, the “relevant range” also changes; when the guess is lower than the generated value, the console should alert the user and change the new upper limit to the guessed value. Conversely, when the guess is higher, the console should alert the user and change the new lower limit to the guessed value. Each time the user is prompted to enter a new number, they should be alerted of the relevant range.	       

Example: 

> Enter a number between 0 and 100: 

>> 64 

> Too high! 

> Enter a number between 0 and 64: 

>> 8 

> Too low! 

> Enter a number between 8 and 64: 